from setuptools import setup, find_packages

setup(
    name="deep-text",
    version="1.3.7",
    keywords=("deep-text", "lstm-crf", "crf", "textrnn", "textcnn", "skip-thoughts", "cdssm", "ocr"),
    description="Deep learning nlp model framework, provides command-line tools.",
    long_description="Deep learning nlp model framework, provides command-line tools.",
    license="GPLv3",

    url="https://gitee.com/wangsihong/deep-text",
    author="wangsihong",
    author_email="wangsihong@live.com",

    packages=find_packages(),
    include_package_data=True,
    platforms="any",
    install_requires=[],

    entry_points={
        'console_scripts': [
            'deeptext_gen_config = common.generate_config:generate_config_file',
            'deepcrf_learn = deepcrf.main:main_learn',
            'deepcrf_eval = deepcrf.main:main_eval',
            'deepcrf_save = deepcrf.main:main_save',
            'deepcls_learn = deepcls.main:main_learn',
            'deepcls_save = deepcls.main:main_save',
            'deeplm_learn = deeplm.main:main_learn',
            'deeplm_save = deeplm.main:main_save',
            'deepmatch_learn = deepmatch.main:main_learn',
            'deepmatch_save = deepmatch.main:main_save',
            'deepemb_learn = deepemb.main:main_learn',
            'deepemb_save = deepemb.main:main_save',
            'deepocr_learn = deepocr.main:main_learn',
            'deepocr_save = deepocr.main:main_save',
        ]
    },
    classifiers=[
        'Development Status :: 4 - Beta',
        'Operating System :: OS Independent',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python',
        'Programming Language :: Python :: Implementation',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Topic :: Software Development :: Libraries'
    ],
    zip_safe=False
)
